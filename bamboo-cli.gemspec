Gem::Specification.new do |s|
  s.name        = "bamboo-cli"
  s.version     = '0.0.5'
  s.authors     = 'James Dumay'
  s.email       = ['james@atlassian.com']

  s.summary     = 'Command line interface for Atlassian Bamboo'
  s.description = 'Command line interface for Atlassian Bamboo'
  s.homepage    = 'https://bitbucket.org/i386/bamboo-cli/overview'

  #dependencies, lets keep'em lean!
  s.add_dependency('thor', '>= 0.14.6')
	s.add_dependency('httparty', '>= 0.8.1')
	s.add_dependency('json', '>= 1.6.5')
	s.add_dependency('uri-handler', '>= 1.0.2')

  s.executables = 'bamboo-cli'
  s.files = ['lib/cli.rb', 'lib/bamboo.rb', 'lib/outputs.rb']
end
